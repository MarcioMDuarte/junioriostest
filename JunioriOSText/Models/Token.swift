//
//  InstagramToken.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 15/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Token: Object, Mappable
{
    static var shared: Token? {
        get{
            let realm = try! Realm()
            return realm.objects(Token.self).first
        }
    }
    
    // MARK: - Properties
    @objc dynamic var id: String?
    
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    
    // MARK: - Mappers
    func mapping (map: Map) {
        self.id <- map["access_token"]
    }
    // MARK: - Realm management
    override static func primaryKey() -> String? {
        return "id"
    }
}

