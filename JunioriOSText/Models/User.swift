//
//  User.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 15/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Data: Object, Mappable
{
    static var shared: Data? = nil
    var id: Int64 = 0
    var data: User?
    var meta: String?
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    func mapping (map: Map) {
        self.data <- map["data"]
        self.meta <- map["meta"]
    }
    // MARK: - Realm management
    override static func primaryKey() -> String? {
        return "id"
    }
}

class User: Object, Mappable
{
    
    static var shared: User? = nil
    
    // MARK: - Properties
    @objc dynamic var id: Int64 = 0
    @objc dynamic var username: String? = nil
    @objc dynamic var profile_picture: String? = nil
    @objc dynamic var full_name: String? = nil
    @objc dynamic var is_business: Bool = false
    @objc dynamic var counts: Counts?
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    // MARK: - Mappers
    func mapping (map: Map) {
        self.username <- map["username"]
        self.profile_picture <- map["profile_picture"]
        self.full_name <- map["full_name"]
        self.is_business <- map["is_business"]
        self.counts <- map["counts"]
    }
    
    // MARK: - Realm management
    override static func primaryKey() -> String? {
        return "id"
    }
}

class Counts: Object, Mappable
{
    static var shared: Counts? = nil
    @objc dynamic var media: User?
    @objc dynamic var follows: Int64 = 0
    @objc dynamic var followed_by: Int64 = 0
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    func mapping (map: Map) {
        self.media <- map["media"]
        self.follows <- map["fallows"]
        self.followed_by <- map["followed_by"]
    }
}
