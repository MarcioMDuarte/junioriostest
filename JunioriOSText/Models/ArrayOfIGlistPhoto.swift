//
//  ArrayOfIGlistPhoto.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 17/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import IGListKit

class ArrayOfIGlistPhoto: ListDiffable {
    
    var id : String
    var arrayOfIgListPhotos: [IGListPhoto]
    init(id: String,arrayOfIgListPhotos: [IGListPhoto]) {
        self.id = id
        self.arrayOfIgListPhotos = arrayOfIgListPhotos
    }
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return true //compare your object here, I returned true for test
    }
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
}
