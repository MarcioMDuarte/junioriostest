//
//  IGListPhoto.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 16/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import IGListKit

class IGListPhoto: ListDiffable {
    
    private var identifier: String = UUID().uuidString
    
    private(set) var thumbnail: String
    private(set) var low_resolution: String
    private(set) var standard_resolution: String
    private(set) var type: String
    private(set) var link: String
    private(set) var identifierPhoto: String
    
    init(type: String, link: String, thumbnail: String, low_resolution: String, standard_resolution: String, identifier: String) {
        self.type = type
        self.link = link
        self.thumbnail = thumbnail
        self.low_resolution = low_resolution
        self.standard_resolution = standard_resolution
        self.identifierPhoto = identifier
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return identifier as NSString
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object as? IGListPhoto else {
            return false
        }
        
        return self.identifier == object.identifier
    }
}
