//
//  Photos.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 16/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class ArrayOfData: Mappable
{
    static var shared: ArrayOfData? = nil

    var data: [Photos]?
    var meta: String?
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    func mapping (map: Map) {
        self.data <- map["data"]
        self.meta <- map["meta"]
    }
}

class Photos: Mappable
{
    
    static var shared: Photos? = nil
    
    // MARK: - Properties
    var id: Int64 = 0
    var type: String? = nil
    var link: String? = nil
    var identifier: String? = nil
    var image: Images?
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    // MARK: - Mappers
    func mapping (map: Map) {
        self.type <- map["type"]
        self.link <- map["link"]
        self.image <- map["images"]
        self.identifier <- map["id"]
        
    }
}
class Images: Object, Mappable
{
    
    static var shared: Images? = nil
    
    // MARK: - Properties
    var thumbnail: URLPhotos?
    var low_resolution: URLPhotos?
    var standard_resolution: URLPhotos?
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    // MARK: - Mappers
    func mapping (map: Map) {
        self.thumbnail <- map["thumbnail"]
        self.low_resolution <- map["low_resolution"]
        self.standard_resolution <- map["standard_resolution"]
        
    }
}

class URLPhotos: Object, Mappable
{
    static var shared: URL? = nil
    
    // MARK: - Properties
    var url: String? = nil
    
    // MARK: - CTORs
    required convenience init? (map: Map) {
        self.init()
    }
    
    // MARK: - Mappers
    func mapping (map: Map) {
        self.url <- map["url"]
    }
}
