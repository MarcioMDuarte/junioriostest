//
//  APICallComponents.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 16/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import Foundation

struct APICallComponents {
    var headers: [String:String]?
    var parameters: [String:Any]?
    var body: [String:Any]?
    
    mutating func add (headers: [String:String])
    {
        self.headers = headers
    }
    mutating func add (parameters: [String:Any])
    {
        self.parameters = parameters
    }
    mutating func add (body: [String:Any])
    {
        self.body = body
    }
}
