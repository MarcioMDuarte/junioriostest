//
//  ViewImageViewController.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 17/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import UIKit

class ViewImageViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    
    var photo:IGListPhoto?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.photoImageView.kf.setImage(with: URL(string: (photo?.standard_resolution)!))

    }

}
