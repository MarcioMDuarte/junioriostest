//
//  ViewController.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 14/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import InstagramKit
import Kingfisher
import Realm
import RealmSwift
import IGListKit

class ViewController: UIViewController, ListAdapterDataSource , InstagramPreviewSectionControllerDelegate{
    
    
    //IBOutlets
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    
    @IBOutlet weak var instagramPhotosCollectionView: UICollectionView!
    // MARK: - Properties
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    var data: [Any] = []
    var photos: [Photos]?
    var object: [ListDiffable] = []
    var items : [Any] = []
    var photosCollection : ArrayOfIGlistPhoto?
    /// refresh control.
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.photosCollection = ArrayOfIGlistPhoto(id: "1", arrayOfIgListPhotos: self.object as! [IGListPhoto])
        self.items.append(photosCollection as Any)
        adapter.collectionView?.frame = instagramPhotosCollectionView.bounds
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        adapter.collectionView?.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: width / 2, height: width / 2)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        adapter.collectionView?.collectionViewLayout = layout
        adapter.collectionView = instagramPhotosCollectionView
        adapter.dataSource = self
        adapter.performUpdates(animated: true)
   
        //ConfigView
        self.ConfigView()
    }

    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return items as! [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        var sectionController: ListSectionController
        
        sectionController = InstagramPreviewSectionController(elements: self.photosCollection as! ArrayOfIGlistPhoto)
        (sectionController as! InstagramPreviewSectionController).delegate = self
        return sectionController
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
    
    func ConfigView()
    {
        let realm = try! Realm()
        var user = realm.objects(User.self).first
        self.informationView.backgroundColor = UIColor(red:0.16, green:0.17, blue:0.18, alpha:1.0)
        
        self.userImageView.layer.masksToBounds = false
        self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height/2
        self.userImageView.clipsToBounds = true
        self.userImageView.kf.setImage(with: URL(string: (user?.profile_picture)!), placeholder: #imageLiteral(resourceName: "genericuser"), options: nil, progressBlock: nil, completionHandler: nil)
        self.userNameLabel.text = user?.username
        self.userNameLabel.textColor = UIColor.white
    }
    
    // MARK: - InstagramPreviewSectionController
    func didSelectedItem(_ sectionController: InstagramPreviewSectionController, photo: IGListPhoto) {
        performSegue(withIdentifier: "showPhoto", sender: photo)
    }
    @IBAction func logout(_ sender: Any)
    {
        let realm = try! Realm()
        
        try! realm.write
        {
            realm.deleteAll()
        }
        if let window = UIApplication.shared.windows.first
        {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginWithInstagramViewController") as? LoginWithInstagramViewController
            {
                window.rootViewController = viewController
                window.makeKeyAndVisible()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoto"
        {
            if let viewController = segue.destination as? ViewImageViewController {
                viewController.photo = sender as! IGListPhoto
            }
        }
    }
}
