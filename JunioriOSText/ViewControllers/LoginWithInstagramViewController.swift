//
//  LoginWithInstagram.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 15/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import UIKit
import InstagramKit
import RealmSwift
import WebKit
import IGListKit

class LoginWithInstagramViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    @IBOutlet weak var webViewInstagramLogin: WKWebView!
    var webView: WKWebView!
    
    var photos: [Photos]?
    var object: [ListDiffable] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Token.shared?.id == nil
        {
            let webConfiguration = WKWebViewConfiguration()
            self.webView = WKWebView(frame: .zero, configuration: webConfiguration)
            self.webView.navigationDelegate = self
            view = self.webView
            let auth = InstagramEngine.shared().authorizationURL()
            self.webView.load(URLRequest(url: auth))
        }
        else
        {
            JunioriOSTestSDK.service.getInstagramUserProfile(withToken: (Token.shared?.id)!) { (data, success) in
                if success
                {
                    let user: User = (data?.data)!
                    
                    let realm = try! Realm()
                    try! realm.write
                    {
                        realm.add(user, update: true)
                    }
                    JunioriOSTestSDK.service.getInstagramPhotos(whitToken: (Token.shared?.id)!) { (data: [ArrayOfData]?, sucess) in
                        if sucess
                        {
                            self.photos = data![0].data
                            for photo in self.photos!
                            {
                                let list = IGListPhoto(type: photo.type!, link: photo.link!, thumbnail: (photo.image?.thumbnail?.url)!, low_resolution: (photo.image?.low_resolution?.url)!, standard_resolution: (photo.image?.standard_resolution?.url)!, identifier: photo.identifier!)
                                self.object.append(list)
                            }
                            
                            self.rootToHomeViewController(whitPhotos: self.object)
                        }
                    }
                }
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        var action: WKNavigationActionPolicy?
        
        defer {
            decisionHandler(action ?? .allow)
        }
        
        do {
            
            if let url = navigationAction.request.url {
                if (String(describing: url).range(of: "access") != nil){
                    try InstagramEngine.shared().receivedValidAccessToken(from: url)
                    //If successfully logged in, Access token can be get from here
                    if let accessToken = InstagramEngine.shared().accessToken {
                        print("accessToken: \(accessToken)")
                        let token = Token()
                        let realm = try! Realm()
                        try! realm.write {
                            token.id = accessToken
                            realm.add(token, update: true)
                        }
                        self.dismiss(animated: true, completion: nil)
                        
                        JunioriOSTestSDK.service.getInstagramUserProfile(withToken: (Token.shared?.id)!) { (data, success) in
                            if success
                            {
                                let user: User = (data?.data)!
                                print("asdasdasdasdasdad", user)
                                let realm = try! Realm()
                                try! realm.write {
                                    realm.add(user)
                                }
                                JunioriOSTestSDK.service.getInstagramPhotos(whitToken: (Token.shared?.id)!) { (data: [ArrayOfData]?, sucess) in
                                    if sucess
                                    {
                                        self.photos = data![0].data
                                        for photo in self.photos!
                                        {
                                            let list = IGListPhoto(type: photo.type!, link: photo.link!, thumbnail: (photo.image?.thumbnail?.url)!, low_resolution: (photo.image?.low_resolution?.url)!, standard_resolution: (photo.image?.standard_resolution?.url)!, identifier: photo.identifier!)
                                            self.object.append(list)
                                            
                                        }
                                        self.rootToHomeViewController(whitPhotos: self.object)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func rootToHomeViewController(whitPhotos photos: [ListDiffable])
    {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! ViewController
        vc.object = photos
        let navigationController = UINavigationController(rootViewController: vc)
        self.present(navigationController, animated: true, completion: nil)
    }
}
