//
//  InstagramPreviewSectionController.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 16/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import UIKit
import IGListKit

protocol InstagramPreviewSectionControllerDelegate{
    func didSelectedItem(_ sectionController: InstagramPreviewSectionController, photo: IGListPhoto)
}

class InstagramPreviewSectionController: ListSectionController{
    
    var instagramList: ArrayOfIGlistPhoto?
    var delegate: InstagramPreviewSectionControllerDelegate?
    
    var numberOfElements: Int? = 0
    init(elements: ArrayOfIGlistPhoto)
    {
        self.numberOfElements = elements.arrayOfIgListPhotos.count
    }
    
    override func numberOfItems() -> Int {
        return numberOfElements!
    }
    
    
    override func didUpdate(to object: Any) {
        instagramList = object as? ArrayOfIGlistPhoto
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: (collectionContext?.containerSize.width)! / 2, height: (collectionContext?.containerSize.height)! / 2) //CGSize(width: 180, height: 180)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        var cell: UICollectionViewCell = UICollectionViewCell()
        guard let photo = instagramList?.arrayOfIgListPhotos[index] else {
            return cell
        }
        
        if let photoCell = getphotoCell(at: index){
            cell = photoCell
        }
        
        guard let photoCell = cell as? photosCollectionViewCell else {
            return cell
        }
        
        photoCell.updateCell(photo: photo)
        
        return cell
    }
    
    private func getphotoCell(at index: Int) -> UICollectionViewCell? {
        guard let ctx = collectionContext else {
            return UICollectionViewCell()
        }
        //let nibName = String(describing: photosCollectionViewCell.self)
        //let cell = ctx.dequeueReusableCell(withNibName: nibName , bundle: nil, for: self, at: index)
        let cell = ctx.dequeueReusableCellFromStoryboard(withIdentifier: "photosCollectionViewCell", for: self, at: index)
        return cell
    }
    
    override func didSelectItem(at index: Int) {
        collectionContext?.performBatch(animated: true, updates: { (batchContext) in
            batchContext.reload(self)
            self.delegate?.didSelectedItem(self, photo: (self.instagramList?.arrayOfIgListPhotos[index])!)
        })
        
    }
}
