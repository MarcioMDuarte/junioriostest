//
//  photosCollectionViewCell.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 16/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import UIKit

class photosCollectionViewCell: UICollectionViewCell {
    // MARK: - Outlets
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var firstLabelText: UILabel!
    
    func updateCell(photo: IGListPhoto)
    {
        self.photoImageView.kf.setImage(with: URL(string: photo.low_resolution))
        self.firstLabelText.text = photo.link
    }
    
}
