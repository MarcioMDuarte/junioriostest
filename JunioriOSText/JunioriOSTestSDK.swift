//
//  JunioriOSTestSDK.swift
//  JunioriOSText
//
//  Created by Márcio Duarte on 16/09/2018.
//  Copyright © 2018 Márcio Duarte. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


struct JunioriOSTestSDK
{
    
    private static var alamofireManager: Alamofire.SessionManager = {
        
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager()
        manager.session.configuration.timeoutIntervalForResource = 20
        
        return manager
    }()
    
    
    
    private typealias Endpoint = (url: String, method: HTTPMethod)
    private struct Endpoints
    {
        static let baseURL = "https://api.instagram.com/v1/users/self/"
        static func getUser (withToken token: String) -> Endpoint{
            let endpoint: Endpoint = (url: baseURL + "?access_token=" +  token, method: .get)
            return endpoint
        }
        
        static func getInstagramPhotos (withToken token: String) -> Endpoint{
            let endpoint: Endpoint = (url: baseURL + "media/recent/?access_token=" +  token, method: .get)
            return endpoint
        }
        
    }
    
    struct service
    {
        static func getInstagramUserProfile (withToken: String, didFinish: @escaping (Data?, Bool) -> Void)
        {
            let components = APICallComponents(headers: nil, parameters: nil, body: nil)
            JunioriOSTestSDK.call(Endpoints.getUser(withToken: withToken), withComponents: components)
            {(response, success) in
                didFinish(response, success)
            }
        }
        
        static func getInstagramPhotos <T: Mappable> (whitToken Token: String, didReceive: @escaping ([T]?, Bool) -> Void) {
            let apiCallComponents = APICallComponents(headers: nil, parameters: nil, body: nil)
            JunioriOSTestSDK.getArrayOfPhotos(from: Endpoints.getInstagramPhotos(withToken: Token), withComponents: apiCallComponents) { (array: [T]?, success) in
                didReceive(array, success)
            }
        }
    }
    
    private static func getArrayOfPhotos <T: Mappable> (from endpoint: Endpoint, withComponents components: APICallComponents, didReceive: @escaping ([T]?, Bool) -> Void) {
        var parameters: [String:Any]?
        switch endpoint.method {
        case .get:
            parameters = components.parameters
        case .post, .put:
            parameters = components.body
        default:
            break
        }
        
        
        JunioriOSTestSDK.alamofireManager.request(endpoint.url, method: endpoint.method , parameters: parameters, encoding:  URLEncoding.httpBody, headers: components.headers)
            .validate(statusCode: 200..<400)
            .responseString(completionHandler: { (response) in
                
                print("\nAlamofire.request ===>> \(response.request?.httpMethod ?? "N/A") \(response.request?.url?.absoluteString ?? "N/A")")
                print("\nAlamofire.request response.description ===>> \(response.description)")
                
                if let response = response.result.value {
                    let array = Mapper<T>().mapArray(JSONString: response)
                    didReceive(array,true)
                }
                else
                {
                    didReceive(nil,false)
                }
            })
    }
    
    private static func call <T: Mappable> (_ endpoint: Endpoint, withComponents components: APICallComponents, didReceive: @escaping ( T?, Bool)->Void)
    {
        var parameters: [String:Any]?
        switch endpoint.method {
        case .get:
            parameters = components.parameters
        case .post, .put:
            parameters = components.body
        default:
            break
        }
        
        
        JunioriOSTestSDK.alamofireManager.request(endpoint.url, method: endpoint.method , parameters: parameters, encoding:  URLEncoding.default, headers: components.headers)
            .validate(statusCode: 200..<400)
            .responseJSON {
                response in
                
                print("\nAlamofire.request ===>> \(response.request?.httpMethod ?? "N/A") \(response.request?.url?.absoluteString ?? "N/A")")
                print("\nAlamofire.request response.description ===>> \(response.description)")
                
                switch response.result
                {
                case .success:
                    if let json = response.result.value as? [String:Any], let object = T(JSON: json)
                    {
                        print(object)
                        DispatchQueue.main.async {
                            didReceive(object, true)
                        }
                    }
                case .failure:
                    return
                    DispatchQueue.main.async {
                        didReceive(nil, false)
                    }
                }
        }
    }
}
